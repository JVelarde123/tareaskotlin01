package com.jinex.android.kt.laboratorios.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jinex.android.kt.laboratorios.R
import kotlinx.android.synthetic.main.activity_lab01_1.*

class Lab01_1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab01_1)

        btnProcesar.setOnClickListener {
            val edad = txtEdad.text.toString().toInt()
            if(edad < 17){
                lbResultado.text = "Usted es menor de edad."
            }else{
                lbResultado.text = "Usted es mayor de edad."
            }
        }

    }
}
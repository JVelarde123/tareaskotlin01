package com.jinex.android.kt.laboratorios.Activity.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jinex.android.kt.laboratorios.Activity.model.Persona
import com.jinex.android.kt.laboratorios.R
import com.jinex.android.kt.laboratorios.databinding.ItemRvBinding

class PersonaAdapter (var personas:MutableList<Persona> = mutableListOf()):RecyclerView.Adapter<PersonaAdapter.PersonaAdapterViewHolder>() {
    inner class PersonaAdapterViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        private val binding:ItemRvBinding = ItemRvBinding.bind(itemView)
        fun bind(persona:Persona){
            binding.txtNombre.text = persona.nombre
            binding.txtRaza.text = persona.cargo
            binding.txtPreferencia.text = persona.email
        }
    }

    fun updateList(personas: MutableList<Persona>){
        this.personas = personas
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonaAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_rv,parent,false)
        return PersonaAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: PersonaAdapterViewHolder, position: Int) {
        val persona = personas[position]
        holder.bind(persona)
    }

    override fun getItemCount(): Int {
        return personas.size
    }
}
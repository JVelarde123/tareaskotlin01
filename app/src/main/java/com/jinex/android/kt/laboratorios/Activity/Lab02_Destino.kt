package com.jinex.android.kt.laboratorios.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jinex.android.kt.laboratorios.R
import kotlinx.android.synthetic.main.activity_lab02__destino.*

class Lab02_Destino : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab02__destino)

        val bundle = intent.extras

        bundle?.let {
            txtNombreDesti02.text =it.getString("nombre")
            txtEdadDesti02.text = it.getString("edad")
            txtVacunasDesti02.text = "Cantidad de vacunas: ${it.getInt("vacuna")}"
            val masco = it.getString("mascota")
            when {
                masco.equals("perro") -> {
                    imgMascota.setImageResource(R.drawable.perro)
                }
                masco.equals("gato") -> {
                    imgMascota.setImageResource(R.drawable.gato)
                }
                masco.equals("conejo") -> {
                    imgMascota.setImageResource(R.drawable.conejo)
                }
            }
        }

    }
}
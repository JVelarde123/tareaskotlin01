package com.jinex.android.kt.laboratorios.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jinex.android.kt.laboratorios.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLab01.setOnClickListener {
            startActivity(Intent(this,Lab01_1::class.java))
        }

        btnLab01_2.setOnClickListener {
            startActivity(Intent(this,Lab01_2::class.java))
        }

        btnLab2.setOnClickListener {
            startActivity(Intent(this,Lab02::class.java))
        }

        btnRecycle.setOnClickListener {
            startActivity(Intent(this,Lab03::class.java))
        }
    }
}
package com.jinex.android.kt.laboratorios.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.jinex.android.kt.laboratorios.R
import kotlinx.android.synthetic.main.activity_lab02.*

class Lab02 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab02)

        btnProcesarLab2.setOnClickListener {
            val nombre = txtNombre.text.toString()
            val edad = txtEdadLab2.text.toString()
            var mascota = ""
            var vacuna = 0
            if(nombre.isEmpty()){
                Toast.makeText(this, "Falta campo nombre.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edad.isEmpty()){
                Toast.makeText(this, "Falta campo edad.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            when {
                rbPerro.isChecked -> {
                    mascota = "perro"
                }
                rbGato.isChecked -> {
                    mascota = "gato"
                }
                rbConejo.isChecked -> {
                    mascota = "conejo"
                }
            }

            when {
                chkVacuna1.isChecked ->{
                    vacuna += 1
                }
                chkVacuna2.isChecked ->{
                    vacuna += 1
                }
                chkVacuna3.isChecked ->{
                    vacuna += 1
                }
                chkVacuna4.isChecked ->{
                    vacuna += 1
                }
                chkVacuna5.isChecked ->{
                    vacuna += 1
                }
            }

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
                putString("mascota",mascota)
                putInt("vacuna",vacuna)
            }
            val intent = Intent(this,Lab02_Destino::class.java)
            intent.putExtras(bundle)
            startActivity(intent)

        }
    }
}
package com.jinex.android.kt.laboratorios.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.jinex.android.kt.laboratorios.Activity.adapter.PersonaAdapter
import com.jinex.android.kt.laboratorios.Activity.model.Persona
import com.jinex.android.kt.laboratorios.R
import com.jinex.android.kt.laboratorios.databinding.ActivityLab03Binding

class Lab03 : AppCompatActivity() {

    private lateinit var binding: ActivityLab03Binding
    private lateinit var personaAdapter:PersonaAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLab03Binding.inflate(layoutInflater)
        setContentView(binding.root)
        setupAdapter()
        crearData()
    }

    private fun setupAdapter() {
        personaAdapter = PersonaAdapter()
        binding.rv.adapter = personaAdapter
        binding.rv.layoutManager = LinearLayoutManager(this)
    }

    fun crearData(){
        var data:MutableList<Persona> = mutableListOf<Persona>()
        var p1 = Persona("Kibo Conley","Ac Orci Corporation","semper.egestas.urna@turpis.edu")
        data.add(p1)
        var p2 = Persona("Jonah Owens","Magna Company","dis.parturient@orciPhasellus.co.uk")
        data.add(p2)
        var p3 = Persona("Drew Perkins","Aliquam Nec Enim Inc.","orci@dictum.org")
        data.add(p3)
        var p4 = Persona("Grady Decker","Eu Inc.","in.molestie.tortor@ultrices.co.uk")
        data.add(p4)
        personaAdapter.updateList(data)
    }
}
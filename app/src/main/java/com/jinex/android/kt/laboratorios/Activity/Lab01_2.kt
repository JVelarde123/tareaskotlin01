package com.jinex.android.kt.laboratorios.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jinex.android.kt.laboratorios.R
import kotlinx.android.synthetic.main.activity_lab01_2.*

class Lab01_2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab01_2)

        btnVerificar.setOnClickListener {
            val nacimiento = etPersona.text.toString().toInt()

            var rpta = when (nacimiento) {
                in 1969..1980 -> {
                    "Generacion X"
                }
                in 1981..1993 -> {
                    "Generacion Y"
                }
                in 1994..2010 -> {
                    "Generacion Z"
                }
                else -> {
                    "No pertences a ninguna generacion"
                }
            }
            lbResultado.text = "$rpta"
        }
    }
}